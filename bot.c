#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <concord/interaction.h>
#include <concord/types.h>
#include <concord/user.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>

#define GUILD_ID <enter guild id here>


#define IMAGE_URL "https://github.com/Cogmasters/concord/raw/master/docs/static/social-preview.png"

// This function is called when bot starts up.
void on_ready(struct discord *client, const struct discord_ready *event){

    log_info("Logged in as %s#%s", event->user->username, event->user->discriminator);

    // activity

    struct discord_activity activities[] = {
            {
            .name = "Segfaulting with Concord",
            .type = DISCORD_ACTIVITY_GAME,
            .details = "Fixing some bugs"
            },
    };

    struct discord_presence_update status = {
        .activities =
            &(struct discord_activities){
                .size = sizeof(activities) / sizeof *activities,
                .array = activities,
            },
        .status = "idle",
        .afk = false,
        .since = discord_timestamp(client),
    };

    discord_update_presence(client, &status);    //////////

    struct discord_create_guild_application_command params = {
        .name = "ping", .description = "Pong!"
    };

    struct discord_create_guild_application_command seg_params = {
        .name = "segfault", .description = "Poem." };

    struct discord_create_guild_application_command topic_params = {
        .name = "topic", .description = "Topic"
    };

    struct discord_create_guild_application_command fixed_it_params = {
        .name = "fixedit", .description = "celebrate fixed code lmfao"
    };

    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &fixed_it_params, NULL);


    discord_create_guild_application_command(client, event->application->id,
                                             GUILD_ID, &params, NULL);

    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &seg_params, NULL);
    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &topic_params, NULL);


    struct discord_application_command_option options[] = {

        {
            .type = DISCORD_APPLICATION_OPTION_INTEGER,
            .name = "num1",
            .description = "num1",
            .required = true
        },

        {
            .type = DISCORD_APPLICATION_OPTION_INTEGER,
            .name = "num2",
            .description = "num2",
            .required = true
        },

        {
            .type = DISCORD_APPLICATION_OPTION_STRING,
            .name = "operator",
            .description = "op",
            .required = true
        }
    };

    struct discord_application_command_option name_options[] = {

        {
            .type = 4,
            .name = "name",
            .description = "your name",
            .required = true
        }

    };

    struct discord_create_guild_application_command add_params = {
        .name = "math",
        .description = "does some math",

        .options = &(struct discord_application_command_options){
            .size = sizeof(options) / sizeof *options,
            .array = options
            }
    };
    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &add_params, NULL);

    struct discord_create_guild_application_command say_name = {
        .name = "sayname",
        .description = "name!",

        .options = &(struct discord_application_command_options){
                .size = sizeof(name_options) / sizeof *name_options,
                .array = name_options
            }
    };
    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &say_name, NULL);

    // embed
    struct discord_create_guild_application_command make_embed = {
        .name = "makeembed",
        .description = "Embedding an embed. Muchos cool."
    };
    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &make_embed, NULL);

    struct discord_application_command_option user_info_options[] = {
        {
            .type = DISCORD_APPLICATION_OPTION_STRING,
            .name = "userid",
            .description = "User's id",
            .required = true
        }
    };

    struct discord_create_guild_application_command user_info = {
        .name = "userinfo",
        .description = "Grabs user info",

        .options = &(struct discord_application_command_options){
            .size = sizeof(user_info_options) / sizeof *user_info_options,
            .array = user_info_options
        }
    };
    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &user_info, NULL);

    struct discord_create_guild_application_command user_avatar = {
        .name = "av",
        .description = "Embeds user avatar.",

        .options = &(struct discord_application_command_options){
            .size = sizeof(user_info_options) / sizeof *user_info_options,
            .array = user_info_options
        }
    };
    discord_create_guild_application_command(client, event->application->id, GUILD_ID, &user_avatar, NULL);
}


// Muchos nice, let's implement the "ping" command
void on_interaction(struct discord *client, const struct discord_interaction *event){
    if(event->type != DISCORD_INTERACTION_APPLICATION_COMMAND)
        return; // return if a msg is not a slash command

    if(strcmp(event->data->name, "av") == 0){
        struct discord_ret_user return_user = {};
        struct discord_user user = {};

        char user_id_string[500];
        u_long user_id = 0;

        for(int i = 0; i < event->data->options->size; ++i){
            char* name = event->data->options->array[i].name;
            char *value = event->data->options->array[i].value;

            if(strcmp(name, "userid") == 0){
                sprintf(user_id_string, "%s", value);
            }
        }

        char *buff;
        user_id = strtol(user_id_string, &buff, user_id);

        return_user.sync = &user;
        discord_get_user(client, user_id, &return_user);

        char user_avatar[256];
        snprintf(user_avatar, sizeof(user_avatar), "https://cdn.discordapp.com/avatars/%lu/%s.jpeg", user_id, user.avatar);

        struct discord_embed embed = {
            .color = 0x6C6F93
        };

        discord_embed_set_image(&embed, user_avatar, NULL, 0, 0);

        struct discord_interaction_response params = {
        .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
        .data = &(struct discord_interaction_callback_data){
        .embeds = &(struct discord_embeds){
            .size = 1,
            .array = &embed
        }
            }
        };

        discord_create_interaction_response(client, event->id, event->token, &params, NULL);

    }

    // user_info
    if(strcmp(event->data->name, "userinfo") == 0){
        struct discord_ret_user return_user = {};
        struct discord_user user = {};

        char user_id_string[500];
        u_long user_id = 0;

        for(int i = 0; i < event->data->options->size; ++i){
            char* name = event->data->options->array[i].name;
            char *value = event->data->options->array[i].value;

            if(strcmp(name, "userid") == 0){
                sprintf(user_id_string, "%s", value);
            }
        }

        char *buff;
        user_id = strtol(user_id_string, &buff, user_id);

        return_user.sync = &user;
        discord_get_user(client, user_id, &return_user);

        char username[256];
        sprintf(username, "Username: %s", user.username);

        char is_bot[20];
        snprintf(is_bot, sizeof(is_bot), "%s", user.bot == 1 ? "true" : "false");

        char user_avatar[256];
        snprintf(user_avatar, sizeof(user_avatar), "https://cdn.discordapp.com/avatars/%lu/%s.jpeg", user_id, user.avatar);

        struct discord_embed embed = {
            .color = 0x6C6F93
        };

        discord_embed_set_title(&embed, "Userinfo");

        discord_embed_add_field(&embed, "Username", username, false);
        discord_embed_add_field(&embed, "Bot?", is_bot, false);

        discord_embed_set_image(&embed, user_avatar, NULL, 500, 500);
        //discord_embed_set_footer(&embed, "Avatar", user_avatar, NULL);
        //discord_embed_set_thumbnail(&embed, user_avatar, NULL, 256, 256);
        discord_embed_set_author(&embed, user.username, NULL, user_avatar, NULL);

        struct discord_interaction_response params = {
        .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
        .data = &(struct discord_interaction_callback_data){
        .embeds = &(struct discord_embeds){
            .size = 1,
            .array = &embed
        }
            }
        };

        discord_create_interaction_response(client, event->id, event->token, &params, NULL);
    }
    // END OF USER INFO //

    //embed

    if(strcmp(event->data->name, "makeembed") == 0){
        struct discord_embed embed = {
            .color = 0xE95379,
            .timestamp = discord_timestamp(client)
        };

        discord_embed_set_title(&embed, "Embed");
        discord_embed_set_description(&embed, "Is it working?");

        discord_embed_set_image(&embed, IMAGE_URL, NULL, 0, 0);

        discord_embed_add_field(
            &embed, "Bot",
            "This is a bot made in C with Concord wraper.",
            false
        );

        discord_embed_add_field(
            &embed, "Language",
            "Who says that C shouldn't be used for anything other than OS dev?",
            false
        );


        struct discord_ret_user return_user = {};
        struct discord_user user = {};

        return_user.sync = &user;
        discord_get_current_user(client, &return_user);

        char username[256];
        sprintf(username, "%s", user.username);

        char is_bot[6];
        sprintf(is_bot, "%d", user.bot);

        discord_embed_add_field(&embed, "Username", username, false);
        discord_embed_add_field(&embed, "Is bot?", is_bot, false);

        struct discord_interaction_response params = {
        .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
        .data = &(struct discord_interaction_callback_data){
        .embeds = &(struct discord_embeds){
            .size = 1,
            .array = &embed
        }
            }
        };

        discord_create_interaction_response(client, event->id, event->token, &params, NULL);
    }

    // end of embed

    if(strcmp(event->data->name, "ping") == 0){
        // response
        struct discord_interaction_response params = {
        .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
        .data = &(struct discord_interaction_callback_data){
             .content = "PONG MOTHERFUCKER!"
           }
        };
        discord_create_interaction_response(client, event->id, event->token, &params, NULL);
    }
    // end of ping

    if(strcmp(event->data->name, "segfault") == 0){
        // response
        struct discord_interaction_response seg_params = {
            .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
            .data = &(struct discord_interaction_callback_data){
                .content = "Try to dereference a void pointer."

        }};
        discord_create_interaction_response(client, event->id, event->token, &seg_params, NULL);
    }
    // end of segfault

    if(strcmp(event->data->name, "topic") == 0){
        char *topics[] = {
            "Language for programming beginners?",
            "Is Linux good?",
            "What OS do you main?",
            "Can you still say C/C++?",
            "Should Windows use FSH?"
        };

        srand(time(0));
        int random_num = rand() % 6;
        char* random_topic = topics[random_num];

        struct discord_interaction_response topic_params = {
            .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
            .data = &(struct discord_interaction_callback_data){
                .content = random_topic
            }
        };

        discord_create_interaction_response(client, event->id, event->token, &topic_params, NULL);
    }
    // end of topic

    if(strcmp(event->data->name, "math") == 0){

        unsigned long num1 = 0;
        unsigned long num2 = 0;
        char operator[2];

        for(int i = 0; i < event->data->options->size; ++i){
            char* name = event->data->options->array[i].name;
            char *value = event->data->options->array[i].value;

            if(strcmp(name, "num1") == 0){
                sscanf(value, "%" SCNu64, &num1);
            }
            else if(strcmp(name, "num2") == 0){
                sscanf(value, "%" SCNu64, &num2);
            }
            else if(strcmp(name, "operator") == 0){
                snprintf(operator, sizeof(operator), "%s", value);
            }
        }
        unsigned long result = 0;

        if(strcmp(operator, "+") == 0){
            result = num1 + num2;
        }
        else if(strcmp(operator, "-") == 0){
            result = num1 - num2;
        }
        else if(strcmp(operator, "*") == 0){
           result = num1 * num2;
        }
        else if(strcmp(operator, "/") == 0){
           result = num1 / num2;
        }

        char str_result[256];

        sprintf(str_result, "Result is: %lu", result);


        struct discord_interaction_response params = {

            .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
            .data = &(struct discord_interaction_callback_data){.content = str_result}

        };

        discord_create_interaction_response(client, event->id, event->token, &params, NULL);
    }

    if(strcmp(event->data->name, "fixedit") == 0){
        struct discord_interaction_response params = {
            .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
            .data = &(struct discord_interaction_callback_data){.content = "Fixed it! :partying_face:"}
        };

        discord_create_interaction_response(client, event->id, event->token, &params, NULL);
    }

    if(strcmp(event->data->name, "sayname") == 0){
        char op[50] = "";

        for(int i = 0; i < event->data->options->size; ++i){
            char* name = event->data->options->array[i].name;
            char *value = event->data->options->array[i].value;

            if(strcmp(name, "name") == 0){
                snprintf(op, sizeof(op), "%s", value);
            }
        }

        char str_response[500];
        snprintf(str_response, sizeof(op), "Your name is: %s", op);

        struct discord_interaction_response params = {
            .type = DISCORD_INTERACTION_CHANNEL_MESSAGE_WITH_SOURCE,
            .data = &(struct discord_interaction_callback_data){.content = str_response}
        };

        discord_create_interaction_response(client, event->id, event->token, &params, NULL);
    }

}


// Finally, the main function
int main(void){

    const char *config_file;
    config_file = "./config.json";

    struct discord *client = discord_config_init(config_file);
    ccord_global_init();
    discord_set_on_ready(client, &on_ready);
    discord_set_on_interaction_create(client, &on_interaction);
    discord_run(client);
    return 0;
}
