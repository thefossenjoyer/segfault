#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <concord/log.h>
#include <string.h>

// COMMANDS //
#include "commands/ping.c"
#include "commands/sayname.c"
#include "commands/poem.c"
#include "commands/maths.c"
#include "commands/embed.c"
#include "commands/ban_unban.c"
#include "commands/mute.c"
#include "commands/clear.c"
// END OF COMMANDS IMPORT //

void on_ready(struct discord *client, const struct discord_ready *event){
    log_info("Logged in as %s#%s", event->user->username, event->user->discriminator);
}

int main(int argc, char **argv){
    const char *config;

    if(argc > 1)
        config = argv[1];
    else
        config = "config.json";

    struct discord *client = discord_config_init(config);

    discord_set_on_ready(client, on_ready);

    discord_set_on_command(client, "!ping", ping);
    discord_set_on_command(client, "!sayname", sayname);
    discord_set_on_command(client, "!poem", poem);
    discord_set_on_command(client, "!maths", maths);
    discord_set_on_command(client, "!userinfo", userinfo);
    discord_set_on_command(client, "!ban", ban);
    discord_set_on_command(client, "!unban", unban);
    discord_set_on_command(client, "!mute", mute);
    discord_set_on_command(client, "!unmute", unmute);

    discord_set_on_command(client, "!clear", clear);

    discord_run(client);

}
