#include <concord/discord-response.h>
#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <concord/types.h>

void clear(struct discord *client, const struct discord_message *event){
    u64snowflake message_id = 0ULL;

    sscanf(event->content, "%lu", &message_id);

    discord_delete_message(client, event->channel_id, message_id, NULL);
}
