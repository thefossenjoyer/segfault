#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <stdio.h>
#include <string.h>

void sayname(struct discord *client, const struct discord_message *event){
    char name[500];
    sprintf(name, "%s", event->content);

    char message[500];
    sprintf(message, "Your name is, %s", name);

    struct discord_create_message params = {
        .content = message
    };

    discord_create_message(client, event->channel_id, &params, NULL);
}
