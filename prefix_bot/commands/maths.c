#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void maths(struct discord *client, const struct discord_message *event){

    unsigned long num1 = 0;
    unsigned long num2 = 0;
    char op[5];

    unsigned long result = 0;

    char message[700];

    int argc = sscanf(event->content, "%lu %lu %s", &num1, &num2, op);

    if(argc < 3){
        struct discord_create_message params = {.content = "Not enough args passed."};
        discord_create_message(client, event->channel_id, &params, NULL);
    }


    if(strcmp(op, "+") == 0) {
        result = num1 + num2;
    }

    else if(strcmp(op, "-") == 0) {
        result = num1 - num2;
    }

    else if(strcmp(op, "*") == 0) {
        result = num1 * num2;
    }

    else if(strcmp(op, "/") == 0) {
        result = num1 / num2;
    }

    sprintf(message, "Result is: %lu", result);

    struct discord_create_message params = {
        .content = message
    };

    discord_create_message(client, event->channel_id, &params, NULL);
}
