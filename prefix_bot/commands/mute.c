#include <concord/discord_codecs.h>
#include <concord/error.h>
#include <concord/types.h>
#include <stdio.h>
#include <inttypes.h>
#include <concord/discord.h>

void mute(struct discord *client, const struct discord_message *event){

    u64snowflake user_id = 0ULL;
    u64snowflake muted_role = 1005345844867903518;

    sscanf(event->content, "%lu", &user_id);

    struct discord_create_message message = {.content = "Muted the user."};

    struct discord_ret_guild_member return_member = {0};
    struct discord_guild_member member = {0};

    return_member.sync = &member;
    discord_get_guild_member(client, event->guild_id, user_id, &return_member);

    int err_code = 0;

    for(int i = 0; i < member.roles->size; i++){
        if(member.roles->array[i] == muted_role){
            err_code = 1;
        }
    }

    if(err_code == 1){
        struct discord_create_message error_message = {.content = "User is already muted."};
        discord_create_message(client, event->channel_id, &error_message, NULL);
    } else {
        discord_add_guild_member_role(client, event->guild_id, user_id, muted_role, NULL);
        discord_create_message(client, event->channel_id, &message, NULL);
    }


}

void unmute(struct discord *client, const struct discord_message *event){

    u64snowflake user_id = 0ULL;
    u64snowflake muted_role = 1005345844867903518;

    sscanf(event->content, "%lu", &user_id);

    struct discord_ret_guild_member return_member = {0};
    struct discord_guild_member member = {0};

    return_member.sync = &member;
    discord_get_guild_member(client, event->guild_id, user_id, &return_member);

    int err_code = 0;

    struct discord_create_message message = {.content = "Unmuted the user."};

    for(int i = 0; i < member.roles->size; i++){
        if(member.roles->array[i] != muted_role){
            err_code = 1;
        }
        else {
            err_code = 0;
        }
    }

    if(err_code == 1){
        struct discord_create_message error_message = {.content = "User is not muted."};
        discord_create_message(client, event->channel_id, &error_message, NULL);
    } else {
        discord_remove_guild_member_role(client, event->guild_id, user_id, muted_role, NULL);
        discord_create_message(client, event->channel_id, &message, NULL);
    }

}
