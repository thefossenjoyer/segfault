#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <concord/types.h>
#include <inttypes.h>

void ban(struct discord *client, const struct discord_message *event){

    u64snowflake user_id = 0ULL;
    char reason[256];

    sscanf(event->content, "%lu %s", &user_id, reason);

    struct discord_create_guild_ban params = {
        .delete_message_days = 0,
        .reason = reason
    };

    char message[500];
    sprintf(message, "The user has been banned. Reason: %s", reason);

    struct discord_create_message message_params = {.content = message};

    discord_create_guild_ban(client, event->guild_id, user_id, &params, NULL);
    discord_create_message(client, event->channel_id, &message_params, NULL);

}

void unban(struct discord *client, const struct discord_message *event){

    u64snowflake user_id = 0ULL;
    char reason[256];

    sscanf(event->content, "%lu", &user_id);

    struct discord_create_message message_params = {.content = "The user has been unbanned."};

    discord_remove_guild_ban(client, event->guild_id, user_id, NULL);
    discord_create_message(client, event->channel_id, &message_params, NULL);
}
