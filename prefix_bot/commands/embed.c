#include <concord/discord.h>
#include <concord/discord_codecs.h>
#include <concord/interaction.h>
#include <concord/types.h>
#include <concord/user.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>
void userinfo(struct discord *client, const struct discord_message *event){

    char userid_str[500] = "";
    char *buff;

    unsigned long user_id = 0;

    sprintf(userid_str, "%s", event->content);

    user_id = strtol(userid_str, &buff, user_id);

    struct discord_ret_guild_member return_member = {};
    struct discord_guild_member member = {};

    struct discord_ret_user return_user = {};
    struct discord_user user = {};

    return_user.sync = &user;
    discord_get_user(client, user_id, &return_user);

    return_member.sync = &member;
    discord_get_guild_member(client, event->guild_id, user_id, &return_member);

    struct discord_embed embed = {.color = 0xE95379};
    discord_embed_set_title(&embed, "Userinfo");

    if(member.nick == NULL){
        member.nick = user.username;
    }

    discord_embed_add_field(&embed, "Username", member.nick, false);

    char is_bot[20];
    sprintf(is_bot, "%s", user.bot == 1 ? "This user is a bot" : "This user is not a bot.");

    discord_embed_add_field(&embed, "Is this user a bot?", is_bot, false);

    char user_avatar[500];
    sprintf(user_avatar, "https://cdn.discordapp.com/avatars/%lu/%s.jpeg", user_id, user.avatar);

    discord_embed_set_image(&embed, user_avatar, NULL, 0, 0);

    struct discord_create_message params = {.embeds = &(struct discord_embeds){
        .size = 1,
        .array = &embed
    }};


    discord_create_message(client, event->channel_id, &params, NULL);
}
